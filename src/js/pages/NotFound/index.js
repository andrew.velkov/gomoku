import React from 'react';
import { Link } from 'react-router-dom';
import Button from 'components/Button';

import css from 'style/pages/NotFound';

const deadTucan = require('assets/img/dead-tucan.png');

export default class NotFound extends React.Component {
  render() {
    return (
      <section className={ css.error } >
        <div className={ css.error__message } >
          <div className={ css.error__img }>
            <img src={ deadTucan } alt='Page not found' />
          </div>
          <div className={ css.error__description }>
            <h1 className={ css.error__main_title }><b>404</b> Oops! Page not found</h1>
            <h2 className={ css.error__title }>Keep calm and return to the previous page.</h2>
            <p className={ css.error__text }>
              Looks like the page you’re trying to visit doesn’t exist. Please check the URL and try your luck again.
            </p>
            <Link to='/'>
              <Button primaryClass={ true }>Back home</Button>
            </Link>
          </div>
        </div>
      </section>
    );
  }
}
