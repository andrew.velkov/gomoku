import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import Button from 'components/Button';

import css from 'style/pages/Main';

let num = 0;

export default class Main extends Component {
  state = {
    blocked: false,
    dot: '0',
    index: [0, 0],
    data: [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    winner: '',
  }

  getSecondDiags = (arr) => {
    return this.getFirstDiags(this.reverseSubArrs(arr));
  }

  getColumns = (arr) => {
    const result = [];

    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < arr[i].length; j++) {
        if (result[j] === undefined) {
          result[j] = [];
        }
        result[j][i] = arr[i][j];
      }
    }

    return result;
  }

  getFirstDiags = (arr) => {
    const result = [];

    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < arr[i].length; j++) {
        if (result[i + j] === undefined) {
          result[i + j] = [];
        }
        result[i + j].push(arr[i][j]);
      }
    }

    return result;
  }

  checkWin = (player, lines) => {
    for (let i = 0; i < lines.length; i++) {
      for (let j = 3; j < lines[i].length; j++) {
        if (
          lines[i][j - 4] === player &&
          lines[i][j - 3] === player &&
          lines[i][j - 2] === player &&
          lines[i][j - 1] === player &&
          lines[i][j] === player
        ) {
          return true;
        }
      }
    }
    return false;
  }

  reverseSubArrs = (arr) => {
    const result = [];

    for (let i = 0; i < arr.length; i++) {
      for (let j = arr[i].length - 1; j >= 0; j--) {
        if (result[i] === undefined) {
          result[i] = [];
        }
        result[i].push(arr[i][j]);
      }
    }

    return result;
  }

  endGame = (winner) => {
    this.setState({ winner });
    this.setState({ blocked: true });
  }

  handleClick = (e, indexRow, indexCol) => {
    e.preventDefault();
    const { data } = this.state;
    const dot = num % 2 === 0 ? 'x' : '0';
    num++;
    const cols = this.getColumns(data);
    const leftDiagonal = this.getFirstDiags(data);
    const rightDiagonal = this.getSecondDiags(data);

    const newData = [...data];
    newData[indexRow].splice(indexCol, 1, dot);

    if (this.checkWin(dot, data.concat(cols, leftDiagonal, rightDiagonal))) {
      this.endGame(dot);
    }

    this.setState({
      index: [indexRow, indexCol],
      data: newData,
      dot,
    });
  }

  reset = () => {
    this.setState({
      data: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      ],
      blocked: false,
      winner: '',
      dot: '0',
    });
  }

  render() {
    const { data, winner, blocked, dot } = this.state;

    return (
      <section className={ css.wrap }>
        <div className={ cx(css.wrap__content, css.wrap__content_small) }>
          <h3 className={ css.wrap__title }>
            <span>Gomoku!</span> &nbsp;
            {winner && <span className={ cx(css.wrap__player, css.wrap__player_winner) }>Winner: { winner }</span>}
            {!winner && <span>След. ход: <i className={ css.wrap__player }>{ dot === '0' ? 'x' : '0' }</i></span>}
          </h3>

          <article className={ cx(css.table, { [css.table_blocked]: blocked }) }>
            {data.map((rows, indexRow) => {
              /* eslint-disable */
              return (
                <ul key={ indexRow } className={ css.table__row }>
                  {rows.map((cols, indexCol) => {
                    return (
                      <Link
                        to='#'
                        key={ indexCol }
                        className={ css.table__cell }
                        onClick={ (e) => this.handleClick(e, indexRow, indexCol) }
                      >
                        { cols !== 0 ? cols : '' }
                      </Link>
                    );
                  })}
                </ul>
              );
            })}
          </article>

          <div className={ cx(css.buttonGroup, css.buttonGroup_center) }>
            <Button primaryClass={ true } onClick={ this.reset }>Reset</Button>
          </div>
        </div>
      </section>
    );
  }
}
