import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { RaisedButton, FlatButton, IconButton, FloatingActionButton } from 'material-ui';

import css from 'style/components/Button';

const Button = ({
  typeButton = 'raise',
  iconName,
  className,
  onClick,
  primaryClass,
  secondaryClass,
  disabledClass,
  children,
  ...button
}) => (
  <span>
    {typeButton === 'raise' &&
      <RaisedButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
    }

    {typeButton === 'flat' &&
      <FlatButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
    }

    {typeButton === 'icon' &&
      <IconButton className={ className } onClick={ onClick } disabled={ disabledClass }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </IconButton>
    }

    {typeButton === 'circle' &&
      <FloatingActionButton onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </FloatingActionButton>
    }
  </span>
);

Button.propTypes = {
  typeButton: PropTypes.string,
  iconName: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  primaryClass: PropTypes.bool,
  secondaryClass: PropTypes.bool,
  disabledClass: PropTypes.bool,
  children: PropTypes.any,
};

export default Button;
