# Gomoku

![alt text](http://i.piccy.info/i9/5555f86814650b083feb585d34763147/1535736317/24972/1260166/Gomoku.png)

```
$ git clone https://gitlab.com/andrew.velkov/gomoku.git
```

```
$ cd gomoku
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start
```

Visit `http://localhost:3003/` 