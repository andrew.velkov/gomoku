/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
require('dotenv').config();
const colors = require('colors');

const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const autoprefixer = require('autoprefixer');

let apiUrl = process.env.API_URL;
const devHost = process.env.DEV_HOST || 'localhost';
const devPort = process.env.DEV_PORT || 3003;
const mockupsServer = 'http://localhost:3013';

const nodeEnv = process.env.NODE_ENV || 'development';
const isProduction = nodeEnv === 'production';
const server = process.env.NODE_SERVER || 'local';
const devToolsEnabled = !isProduction && process.env.DEBUG_TOOL_OFF !== 'true';
const mockupsEnabled = process.env.MOCKUPS_ENABLED === 'true';

const buildPath = path.join(__dirname, './build');
const assetsPath = path.join(__dirname, './src/assets');
const sourcePath = path.join(__dirname, './src');
const jsSourcePath = path.join(__dirname, './src/js');
const imgPath = path.join(assetsPath, './img');
const fontsPath = path.join(assetsPath, './fonts');

apiUrl = mockupsEnabled ? mockupsServer : apiUrl;

if (!isProduction) {
  console.log(`For change API URL enter ${colors.bgGreen('set API_URL={your_url}')}`);
  console.log(`For change development host enter ${colors.bgGreen('set DEV_HOST={your_host}')} or port ${colors.bgGreen('set DEV_PORT={your_port}')}`);
  console.log(`For disable Redux DevTools ${colors.bgGreen('set DEBUG_TOOL_OFF=true')}`);
  console.log(`Or set in ${colors.bgGreen('.env')} file \n`);
  console.log(`Environment = ${colors.green(nodeEnv)}`);
  console.log(`Redux DevTools enabled = ${colors.green(devToolsEnabled)}`);
  console.log(`Project use API URL ${colors.green(apiUrl)} \n`);
}

// Common plugins
const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'assets/js/vendor-[hash].js',
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(nodeEnv),
    },
    '__IS_PRODUCTION__': isProduction,
    '__DEV_TOOL__': devToolsEnabled,
    '__SERVER__': JSON.stringify(server)
  }),
  new webpack.NamedModulesPlugin(),
  new HtmlWebpackPlugin({
    template: path.join(sourcePath, 'index.html'),
    path: buildPath,
    //favicon: imgPath + '/favicons/icon.png',
    filename: 'index.html',
    inject: 'body',
  }),
  new webpack.LoaderOptionsPlugin({
    options: {
      postcss: [
        autoprefixer({
          browsers: [
            'last 3 version',
            'ie >= 10',
          ],
        }),
      ],
      context: sourcePath,
    },
  }),
  new FaviconsWebpackPlugin({
    logo: imgPath + '/favicons/icon.png',
    prefix: 'assets/img/favicons-[hash:base64:5]/',
    emitStats: false,
    statsFilename: 'iconstats-[hash:base64:5].json',
    persistentCache: false,
    inject: true,
    background: '#fff',
    title: 'Video AdExchange Panel',
    icons: {
      android: true,
      appleIcon: true,
      appleStartup: true,
      coast: false,
      favicons: true,
      firefox: true,
      opengraph: false,
      twitter: false,
      yandex: false,
      windows: false
    }
  }),
];

// Common rules
const rules = [
  {
    enforce: "pre",
    test: /\.js$/,
    exclude: /node_modules/,
    loader: "eslint-loader",
  },
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: [
      'babel-loader',
    ],
  },
  {
    test: /\.(png|gif|jpg|svg)$/,
    include: imgPath,
    use: 'url-loader?limit=20480&name=assets/img/[name]-[hash:base64:5].[ext]',
  },
  {
    test: /\.(png|woff|woff2|eot|ttf|svg)$/,
    include: fontsPath,
    use: 'url-loader?limit=20480&name=assets/fonts/[name]-[hash:base64:5].[ext]',
  },
];

if (isProduction) {
  // Production plugins
  plugins.push(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false,
      },
    }),
    new ExtractTextPlugin('assets/css/style-[hash].css')
  );

  // Production rules
  rules.push(
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        //sourceMap check for production
        use: 'css-loader?&modules&url&sourceMap&camelCase=dashes&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader!sass-loader',
      }),
    }
  );
} else {
  // Development plugins
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin()
  );

  // Development rules
  rules.push(
    {
      test: /\.scss$/,
      exclude: /node_modules/,
      use: [
        'style-loader',
        // Using source maps breaks urls in the CSS loader
        // https://github.com/webpack/css-loader/issues/232
        // This comment solves it, but breaks testing from a local network
        // https://github.com/webpack/css-loader/issues/232#issuecomment-240449998
        // 'css-loader?sourceMap',
        'css-loader?&modules&camelCase=dashes&sourceMap&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader!sass-loader',
        'postcss-loader',
        'sass-loader?sourceMap',
      ],
    }
  );
}

module.exports = {
  devtool: isProduction ? 'eval' : 'source-map',
  context: sourcePath,
  entry: {
    js: './js/index.js',
    vendor: [
      'babel-polyfill',
      'es6-promise',
      'immutable',
      'isomorphic-fetch',
      'react-dom',
      'react-redux',
      'react-router',
      'react',
      'redux-thunk',
      'redux',
    ],
  },
  output: {
    path: buildPath,
    publicPath: '/',
    filename: 'assets/js/app-[hash].js',
  },
  module: {
    rules,
  },
  resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx', '.scss'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath,
      jsSourcePath,
    ],
  },
  plugins,
  devServer: {
    contentBase: isProduction ? './build' : './src',
    historyApiFallback: true,
    port: devPort,
    compress: isProduction,
    inline: !isProduction,
    hot: !isProduction,
    host: devHost,
    proxy: {
      '/api/*': {
        target: apiUrl,
        changeOrigin: true,
        secure: false,
        cookieDomainRewrite: devHost,
      },
    },
    stats: {
      assets: true,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: true,
      version: false,
      warnings: true,
      colors: {
        green: '\u001b[32m',
      },
    },
  },
};
